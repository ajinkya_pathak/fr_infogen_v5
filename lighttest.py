import cv2
import numpy as np
import requests
import time

cap=cv2.VideoCapture(0)
time.sleep(2.0)


Brightness = cap.get(cv2.CAP_PROP_BRIGHTNESS) # 13.0
print('Brightness=',Brightness)
Contrast   = cap.get(cv2.CAP_PROP_CONTRAST ) # 12.0
print('Contrast=',Contrast) 
Saturation = cap.get(cv2.CAP_PROP_SATURATION) # 47.0
print('Saturation=',Saturation)
Gain       = cap.get(cv2.CAP_PROP_GAIN)  #-1.0
print(Gain)


def emptyFunction(): 
    pass
   
# def main(): 

# window name 
windowName="Camera control"
cv2.namedWindow(windowName)  
   
# there trackbars which have the name 
# of trackbars min and max value  
cv2.createTrackbar('Brightness', windowName, 0, 100, emptyFunction) 
cv2.createTrackbar('Contrast', windowName, 0, 100, emptyFunction) 
cv2.createTrackbar('Saturation', windowName, 0, 100, emptyFunction)

# Used to open the window 
# till press the ESC key 
while(True):
    ret, frame=cap.read()
    frame=cv2.flip(frame,180)

    cv2.imshow(windowName,frame) 
      
    if cv2.waitKey(1) == 27: 
        break
      
    # values of blue, green, red 
    Brightness = cv2.getTrackbarPos('Brightness', windowName) 
    Contrast = cv2.getTrackbarPos('Contrast', windowName) 
    Saturation = cv2.getTrackbarPos('Saturation', windowName)

    cap.set(cv2.CAP_PROP_BRIGHTNESS,Brightness)
    cap.set(cv2.CAP_PROP_CONTRAST,Contrast)
    cap.set(cv2.CAP_PROP_SATURATION,Saturation)

      
    # merge all three color chanels and 
    # make the image composites image from rgb    
    # image[:] = [blue, green, red] 
    # print(blue, green, red) 
       
cv2.destroyAllWindows() 
  
# # Calling main()          
# if __name__=="__main__": 
#     main() 

# frameSize = (640, 480)
# Initialize mutithreading the video stream.
# vs = VideoStream(src=0, resolution=frameSize,framerate=32).start()
# vs = VideoStream(src=0, resolution=(cvsettings.CAMERA_WIDTH, cvsettings.CAMERA_HEIGHT)).start()

# cap=cv2.VideoCapture(0)

# Brightness = cap.get(cv2.CAP_PROP_BRIGHTNESS)
# print(Brightness)
# Contrast   = cap.get(cv2.CAP_PROP_CONTRAST )
# print(Contrast)
# Saturation = cap.get(cv2.CAP_PROP_SATURATION)
# print(Saturation)
# Gain       = cap.get(cv2.CAP_PROP_GAIN)
# print(Gain)



# time.sleep(2.0)
# timeCheck = time.time()
# while(True):
#     ret, frame = cap.read()
#     # frame = vs.read()
#     frame=cv2.flip(frame,180)
#     frame = imutils.resize(frame, width=640,height=480)

#     cv2.imshow("Frame", frame)
#     key = cv2.waitKey(1) & 0xFF
    
#     # if the `q` key was pressed, break from the loop
#     if key == ord("q"):
#       break
#     print('Rate :>>',1/(time.time() - timeCheck))
#     timeCheck = time.time()

# cv2.destroyAllWindows()
# vs.stop()

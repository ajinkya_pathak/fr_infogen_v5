REQUIREMENTS OF FACE RECOGNITION SYSTEM
  
Hardware:
Pansonic IP cam BL-C131/C111(Face Recognition)
Logitech C170 Webcam (Face Data collection)

Python 3.6.x 

Libraries 
    opencv-contrib-python 
    numpy 
    scipy 
    sk-learn 
    Tensorflow==1.5 
    requests 
    autocrop 

Files: 
    Facenet model file in path FacenetModel/model.pb 
    Haar-Cascade file (haarcascade_frontalface_alt) 
    Embedding file (Extracted_Dict_Infogen.pickle) 
    User name list (EachIndividual_Infogen.pickle) 
    Face recognition python file FRhaarmain.py 
    Embedding making file MakeEmbedhaar.py 
    For collecting Face data(images) use CaptureFace.py 

Execution: 
Face data collection and preprocessing (Webcam is required) 
    python3 CaptureFace.py  
    Enter id, first name, last name 
    Camera will start, display will show live webcam feed 
    Press 'c' to capture image. Press as many times as the number of photos you want to collect 
    Press 's' to crop the image into 500px X 500px size with only face 
    Press 'x' to Exit 

Create Embedding file:
    python3 MakeEmbedhaar.py <path to model18.pb> <folder containing user photos> 

Start Face recognition system :
    python3 FRhaarmain.py <path to model18.pb> 

Update: 07-05-19

haar_FR_IP_User_confirmation.py
Using IP cam along with user confirmation

Individual on getting recognized a new window with current image along with user name will pop up and user can confirm whether FR system recognize correctly or not
For new window:
    Correct recognition Press 'c'
    Incorrect recognition Press 'i'
    Press 'x' to close this window"

haar_FR_usb_multithread.py
Using imutils library that help in achieving multi-threading using VideoStream
For new window:
    Correct recognition Press 'c'
    Incorrect recognition Press 'i'
    Press 'x' to close this window"





import warnings
warnings.filterwarnings("ignore")

from sklearn.metrics.pairwise import pairwise_distances #To find out Pairwise distance between x, y[] matrix
from tensorflow.python.platform import gfile # its file I/O wrapper without thread loacking
from scipy import misc
import tensorflow as tf
import numpy as np
# from mtcnn_model 
# import detect_and_align
import argparse 
import time
import cv2
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import csv
import re
from datetime import datetime
import sys
import json
import argparse
import pickle
import pandas as pd
from pathlib import Path
import shutil
# import skvideo
import logging
# skvideo.setFFmpegPath(r'/home/centos/FaceReco/ffmpeg/ffmpeg-4.1')
# import skvideo.io


class IdentifyPerson():
    """Here the class is created to Keeps track of known persons and calculates id matches"""

    def __init__(self, ImageRootFolder, sess, embeddings, images_placeholder,
                 phase_train_placeholder):
        #print('Loading known identities: ', end='')
        self.ImageRootFolder = ImageRootFolder
        # self.mtcnn = mtcnn
        self.EachIndividualPersons = []

        ModifiedImagePathList = [] #Initialized Image path list 
        ImagePathList = []
        try:
            ListOfPeopleFolder = os.listdir(os.path.expanduser(ImageRootFolder)) # It will con
            for EachIndividualPerson in ListOfPeopleFolder:
                
                id_dir = os.path.join(ImageRootFolder, EachIndividualPerson)
                ImagePathList = ImagePathList + [os.path.join(id_dir, img) for img in os.listdir(id_dir)]
            for i in ImagePathList:
                num=re.sub(r'\\',r"/",i)
                ModifiedImagePathList.append(num) 
        except Exception as e:
            logging.info('naviagting through paths %s'% str(e))
        if (os.path.isfile(r"Extracted_Dict_Infogen.pickle")):
            with (open(r"Extracted_Dict_Infogen.pickle", "rb")) as openfile:
                while True:
                    try:
                        embd=pickle.load(openfile)
                        #embd1=pickle.load(openfile)
                    except EOFError:
                        print('with (open("Extracted_Dict_Infogen.pickle", "rb")) as openfile',e)
                    break


            #ModifiedImagePathList=[r'C:/myproject/Copy_FaceRecognition-master/ids/Ajinkya/Ajinkya0.png', r'C:/myproject/Copy_FaceRecognition-master/ids/Akshay/Akshay0.png', r'C:/myproject/Copy_FaceRecognition-master/ids/Kirti/Kirti0.png',r'C:/myproject/Copy_FaceRecognition-master/ids/Diksha/Diksha0.png',r'C:/myproject/Copy_FaceRecognition-master/ids/Vishal/Vishal0.png',r'C:/myproject/Copy_FaceRecognition-master/ids/Neha/Neha0.png',r'C:/myproject/Copy_FaceRecognition-master/ids/Rahul/Rahul0.png',r'C:/myproject/Copy_FaceRecognition-master/ids/Rahul/Rahul1.png']

        #print('Found %d images in id folder' % len(ModifiedImagePathList))
        try:
            aligned_images, id_ModifiedImagePathList = self.DetectFaceInImage(ModifiedImagePathList)
            # print('hdsufwk')
            # logging.info('self.DetectFaceInImage(ModifiedImagePathList) %s'% str(id_ModifiedImagePathList))
            logging.info('getting feed_dict %s'% str(id_ModifiedImagePathList))
            feed_dict = {images_placeholder: aligned_images, phase_train_placeholder: False}
            self.embeddings = sess.run(embeddings, feed_dict=feed_dict)
        except Exception as e:
            logging.error('getting feed_dict  %s'% str(e))

        if (os.path.isfile(r"Extracted_Dict_Infogen.pickle")):
            embeddingsToWrite=np.vstack((embd, self.embeddings))
            try:
                with open(r'Extracted_Dict_Infogen.pickle','wb') as f:
                    pickle.dump(embeddingsToWrite,f,protocol=0)
            except Exception as e:
                logging.error('during making pickle %s'% str(e))
        else:
            try:
                with open(r'Extracted_Dict_Infogen.pickle','wb') as f:
                    pickle.dump(self.embeddings,f,protocol=0)
            except Exception as e:
                logging.error('during making pickle %s'% str(e))
        root_target_dir=r'movedids'
                
        for src_dir,dirs,files in os.walk(ImageRootFolder):
            for dir_ in dirs:
                src_subdir = os.path.join(src_dir, dir_)
                print(src_subdir)
                try:
                    shutil.move( src_subdir,root_target_dir) 
                except Exception as e:
                    try:
                        root_target_dir1=r'movedRepeatedids'
                        shutil.move( src_subdir,root_target_dir1)

                        logging.error('shutil.move( src_subdir,root_target_dir) %s'% str(e))
                    except Exception as e:
                        try:
                            root_target_dir2=r'movedRepeatedids1'
                            shutil.move( src_subdir,root_target_dir2)

                            logging.error('shutil.move( src_subdir,root_target_dir) %s'% str(e))
                        except Exception as e:
                            logging.error('shutil.move( src_subdir,root_target_dir) %s'% str(e))
                            #os.remove(src_subdir)
        
        for src_dir,dirs,files in os.walk(ImageRootFolder):
            for dir_ in dirs:
                src_subdir = os.path.join(src_dir, dir_)
                #print(src_subdir)
                shutil.move( src_subdir,root_target_dir)
        
    def DetectFaceInImage(self, ModifiedImagePathList):
        aligned_images = []
        id_ModifiedImagePathList = []
        for image_path in ModifiedImagePathList:
            if image_path.lower().endswith(('.png', '.jpg', '.jpeg')):
                try:
                    image = misc.imread(os.path.expanduser(image_path), mode='RGB') #read the each image from the local file system
                except Exception as e:
                    logging.error("image = misc.imread(os.path.expanduser(image_path), mode='RGB') %s"% str(image_path))
                    continue
                #image = misc.imread(os.path.expanduser(image_path), mode='RGB') #read the each image from the local file system
                try:
                    # face_patches, _, _ = detect_and_align.detect_faces(image, self.mtcnn)
                    faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
                    faces = faceCascade.detectMultiScale(image,scaleFactor=1.1,minNeighbors=5,minSize=(30, 30),flags=cv2.CASCADE_SCALE_IMAGE)
                    face_patches=[]
                    for (x,y,w,h) in faces:
                        # cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
                        #roi_gray = gray[y:y+h, x:x+w]
                        roi_color = image[y-10:y+h+15, x-10:x+w+15]
                        aligned = misc.imresize(roi_color, (160, 160), interp='bilinear')
                        # cv2.imshow('aligned',aligned)
                        # cv2.waitKey(1)
                        prewhitened = prewhiten(aligned)
                        prewhitened = prewhiten(prewhitened)
                        # cv2.imshow('pre',prewhitened)
                        # cv2.waitKey(1)
                        # cv2.imshow('prewhitened',prewhitened)
                        # cv2.waitKey(1)
                        #cv2.imshow('Haarcascades',roi_color)
                        face_patches.append(prewhitened)
                        aligned_images = aligned_images + face_patches

                        id_ModifiedImagePathList += [image_path] * len(face_patches)
                        self.EachIndividualPersons += [image_path.split('/')[-2]] * len(face_patches)
                except Exception as e:
                    logging.info('DetectFaceInImage %s'% str(e))
                    logging.error('face_patches, _, _ = detect_and_align.detect_faces(image, self.mtcnn) %s'% str(image_path))

            else:
                logging.error("image_path.lower().endswith(('.png', '.jpg', '.jpeg')) %s"% str(image_path))


        if len(self.EachIndividualPersons)==0:
            try:
                # print('in self.EachIndividualPersons ')
                root_target_dir="shiftNullFile"
                ListOfPeopleFolder2 = os.listdir(os.path.expanduser(self.ImageRootFolder))
                # print(ListOfPeopleFolder2)# It will con
                for EachIndividualPerson2 in ListOfPeopleFolder2:

                    id_dir = os.path.join(self.ImageRootFolder, EachIndividualPerson2)
                    shutil.move( id_dir,root_target_dir)
            except Exception as e:
                logging.error("if len(self.EachIndividualPersons)==0: %s"% str(ListOfPeopleFolder2))
                #print(e)

            sys.exit('1')
        #sys.exit('No valid image')        
        try:
            with open(r'EachIndividual_Infogen.pickle','ab') as mi:
                pickle.dump(self.EachIndividualPersons,mi)
        except Exception as e:
            logging.error('during making pickle self.EachIndividualPersons %s'% str(e))

        return np.stack(aligned_images), id_ModifiedImagePathList #Join a sequence of arrays along a new axis.
    
def LoadFeatureExtractedModel(model):
 
    model_exp = os.path.expanduser(model) #pass the facenet model path
    if (os.path.isfile(model_exp)):
        #print('Loading model filename: %s' % model_exp)
        with gfile.FastGFile(model_exp, 'rb') as f:  # 'gfile' make it more efficient for some backing filesystems.
            graph_def = tf.GraphDef() #'GraphDef' is the class created by the protobuf liberary
            graph_def.ParseFromString(f.read()) 
            tf.import_graph_def(graph_def, name='') # To load the TF Graph
    else:
        raise ValueError('Specify model file, not directory!')


def convertPath(path):
        separator = os.path.sep
        if separator != '/':
            path = path.replace(os.path.sep, '/')
            return path

def prewhiten(x):
    mean = np.mean(x)
    std = np.std(x)
    std_adj = np.maximum(std, 1.0 / np.sqrt(x.size))
    y = np.multiply(np.subtract(x, mean), 1 / std_adj)
    return y

def main(args):

    faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')

    ImagePathList1=[]
    ListOfPeopleFolder1 = os.listdir(os.path.expanduser(args.ImageRootFolder[0])) # It will con
    for EachIndividualPerson in ListOfPeopleFolder1:

        id_dir = os.path.join(args.ImageRootFolder[0], EachIndividualPerson)
        ImagePathList1 = ImagePathList1 + [os.path.join(id_dir, img) for img in os.listdir(id_dir)]
    if len(ImagePathList1) == 0:
        return 'Done'

    with tf.Graph().as_default():
        with tf.Session() as sess:
            # Setup models
            # mtcnn = detect_and_align.create_mtcnn(sess, None) #It calls create_mtcnn function from the detect_and_align file 
            LoadFeatureExtractedModel(args.model) #IT loads the facenet 20170512-110547.pb pre-trained model
            #print("xyz",args.model)
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            #print("embeddings",embeddings)
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
            # Load anchor IDs
            logging.info('before indentify')
            id_data = IdentifyPerson(args.ImageRootFolder[0], sess, embeddings, images_placeholder, phase_train_placeholder)
            logging.info('after identify')
            return 'Done'
            #import skvideo;skvideo.setFFmpegPath(r'E:\MY DOwnloads\ffmpeg\bin');import skvideo.io;import cv2;
            
if __name__ == '__main__':
    now = datetime.now()
    now=now.strftime("%Y-%m-%d-%H-%M-%S")
    logging.basicConfig( filename=r'Logs/MakeEmbedings_'+str(now)+'.log',level=logging.DEBUG,format = '%(asctime)s  %(levelname)-10s %(processName)s  %(name)s %(message)s %(lineno)d')
    #logging.disabled = True
    logging.disable(logging.NOTSET) #to enable logging
    #logging.disable(sys.maxsize)   #to disable logging
    parser = argparse.ArgumentParser() 
    
    parser.add_argument('model', type=str,help='load the model from the specific location')
    parser.add_argument('ImageRootFolder', type=str, nargs='+',help='Image folder at that location')
    # parser.add_argument('outputfolder', type=str,help='output folder at that location')
   
    try:
        csvData1=main(parser.parse_args())
        if csvData1 == 'Done':
            print('1')
    except Exception as e:
        print(e)
        logging.info(e)
    logging.info('$$$$$$$$$$$$$$$$ Done $$$$$$$$$$$$$$$$$$')
    logging.shutdown()
'''
Using IPcam
No mutlithreading
name displayed along the bounding box
On recognition pop out anaother window to get user input thru keyboard
'''
import logging
from scipy import misc
import warnings
warnings.filterwarnings("ignore")
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
from sklearn.metrics.pairwise import pairwise_distances #To find out Pairwise distance between x, y[] matrix
from tensorflow.python.platform import gfile # its file I/O wrapper without thread loacking
# from mtcnn_model import detect_and_align
import numpy as np
import requests
# import detect_and_align
import argparse
import time
import cv2
import csv
import re
from datetime import datetime
import sys
import pickle

distance_treshold = 0.7

class IdData():
    """We Created the class to Keeps track of known identities and calculates id matches"""

    def __init__(self,sess, embeddings, images_placeholder, phase_train_placeholder):
        # print('Loading known identities: ', end='')
        logging.info('class called  %s' % str(datetime.now()))
        self.distance_treshold = 0.75
        # self.id_folder = id_folder
        #self.mtcnn = mtcnn
        self.id_names = []

        with (open("EachIndividual_Infogen.pickle", "rb")) as openfile:
            while True:
                try:
                    self.EachIndividualPersons=pickle.load(openfile)
                except EOFError as e:
                    logging.error(e)
                    break

        with (open("Extracted_Dict_Infogen.pickle", "rb")) as openfile:
            while True:
                try:
                    self.embeddings=pickle.load(openfile)
                except EOFError as e:
                    logging.error(e)
                    break
    
    def FindPersonNameBasedOnEmbeddingDistance(self, embs):
        matching_ids = []
        matching_distances = []
        try:
            distance_matrix = pairwise_distances(embs, self.embeddings)
            logging.info('distance_matrix = pairwise_distances(embs, self.embeddings) %s' % str(datetime.now()))
            for distance_row in distance_matrix:
                min_index = np.argmin(distance_row)
                logging.info('min_index = np.argmin(distance_row) %s' % str(datetime.now())) #Returns the indices of the minimum values along an axis.
                if distance_row[min_index] < self.distance_treshold: # if min_index is less then the threshold i.e., 0.7
                    matching_ids.append(self.EachIndividualPersons[min_index]) # If true then append the name into maching_ids[] list 
                    matching_distances.append(distance_row[min_index])# and also append the distance associated with that image
                    logging.info('min_index = np.argmin(distance_row) %s' % str(datetime.now()))
                else:
                    matching_ids.append(None)
                    matching_distances.append(None)
            return matching_ids, matching_distances  # FindPersonNameBasedOnEmbeddingDistance returns the name and the distance of that perticular image
        except Exception as e:
            logging.error(e)                

def LoadFeatureExtractedModel(model): 
    model_exp = os.path.expanduser(model) #pass the facenet model path
    if (os.path.isfile(model_exp)):
        print('Loading model filename: %s' % model_exp)
        with gfile.FastGFile(model_exp, 'rb') as f:  # 'gfile' make it more efficient for some backing filesystems.
            graph_def = tf.GraphDef() #'GraphDef' is the class created by the protobuf liberary
            graph_def.ParseFromString(f.read()) 
            tf.import_graph_def(graph_def, name='') # To load the TF Graph
    else:
        raise ValueError('Specify model file, not directory!')
        logging.error(sys.exit("model.pb not found"))

def prewhiten(x):
    mean = np.mean(x)
    std = np.std(x)
    std_adj = np.maximum(std, 1.0 / np.sqrt(x.size))
    y = np.multiply(np.subtract(x, mean), 1 / std_adj)
    return y

aligned=0
def main(args):
    global aligned
    #Haar-Cascade classifier file path
    faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')  #lbpcascade_frontalface_improved.xml
    
    # print("payload_size: {}".format(payload_size))
    with tf.Graph().as_default(): # getting the tensorflow graph() function as default mode
        with tf.Session() as sess: #Start tensorflow session

            logging.info('with tf.Session() as sess: %s' % str(datetime.now()))
            LoadFeatureExtractedModel(args.model) #IT loads the facenet 20170512-110547.pb pre-trained model
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
            # Load anchor IDs
            id_data = IdData(sess, embeddings, images_placeholder, phase_train_placeholder)
            # cap=cv2.VideoCapture(0)
            font = cv2.FONT_HERSHEY_SIMPLEX
            
            timeCheck = time.time()
            while(True):
                # ret, frame = cap.read()

                try:
                #import requests
                    img_resp=requests.get('http://192.168.1.7/SnapshotJPEG?Resolution=640x480')
                    img_arr=np.array(bytearray(img_resp.content),dtype=np.uint8)
                    frame = cv2.imdecode(img_arr,1)
                    frame = cv2.flip(frame,180)
                except:
                    print('Chech Network connection for IP cam')

                # Add Logo to live video stream
                logo = cv2.imread('logo.png')
                rows,cols,channels = logo.shape
                logo_gray = cv2.cvtColor(logo,cv2.COLOR_BGR2GRAY)
                # Create a mask of the logo and its inverse mask
                ret, mask = cv2.threshold(logo_gray, 200, 255, cv2.THRESH_BINARY_INV)
                # Now just extract the logo
                mask_inv = cv2.bitwise_not(mask)
                logo_fg = cv2.bitwise_and(logo,logo,mask = mask)
                # To put logo on top-left corner, create a Region of Interest (ROI)
                roi = frame[0:rows, 0:cols ] 
                # Now blackout the area of logo in ROI
                frm_bg = cv2.bitwise_and(roi,roi,mask = mask_inv)
                # Next add the logo to each video frame
                dst = cv2.add(frm_bg,logo_fg)
                frame[0:rows, 0:cols ] = dst

                #To display Date & time
                cv2.putText(frame, dt.strftime("Date: %d/%m/%y Time: %H:%M:%S"),(10,470), font, 0.5, (255,118,0), 1)

                faces = faceCascade.detectMultiScale(frame,scaleFactor=1.1,minNeighbors=5,minSize=(30, 30),flags=cv2.CASCADE_SCALE_IMAGE)
                print('faces>>',faces)
                face_patches=[]
                # cv2.rectangle(frame, (261,174),(457,380),(0,0,0),1,cv2.LINE_4)
                for (x,y,w,h) in faces:
                    print('bounding boox of faces',x,y,w,h)

                    cv2.rectangle(frame, (x-20,y-20), (x+w+20,y+h+20), (255,0,0), 2)
                    # cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2,cv2.LINE_8)
                    #roi_gray = gray[y:y+h, x:x+w]
                    try:
                        roi_color = frame[y-10:y+h+15, x-10:x+w+15]
                        print('shapee roi',roi_color.shape)
                    except:
                        roi_color = frame[y:y+h, x:x+w]
                        print('shapee roi',roi_color.shape)
                    try:
                        aligned = misc.imresize(roi_color, (160, 160), interp='bilinear')
                    except:
                        print("aligned = misc.imresize(roi_color, (160, 160), interp='bilinear')")
                        pass

                    try:
                        prewhitened = prewhiten(aligned)
                        prewhitened = prewhiten(prewhitened)
                        face_patches.append(prewhitened)
                    except:
                        print('aligned')
                        pass
                # try:
                #face_patches, padded_bounding_boxes, landmarks = detect_and_align.detect_faces(frame, mtcnn)
                if len(face_patches) > 0:
                    face_patches = np.stack(face_patches) #we created sequence of array of Face images

                    feed_dict = {images_placeholder: face_patches, phase_train_placeholder: False}
                    print('##################################') # Passing this face arrays as Image Placeholder its  variablewhich will only assign the image information at later stage
                    embs = sess.run(embeddings, feed_dict=feed_dict) #Feed placeholders to the tensorflow graphs using feed_dictionary 

                    print('Matches in frame:',embs.shape)
                    matching_ids, matching_distances = id_data.FindPersonNameBasedOnEmbeddingDistance(embs) #Passing the image embeddings to 'id_data' which is class veriable
                    for (x,y,w,h),matching_id, dist in zip(faces,matching_ids, matching_distances):

                        try:
                            # print('<>-<>-<>',matching_id, dist)
                            
                            welcome="Welcome to Infogen labs\n"
                            #matching_id = re.sub('[_]', ' ', matching_id)
                            if matching_id != None and dist != None and dist <= 0.68:

                                id=matching_id.split('_')
                                match=id[0]+' '+id[1]
                                cv2.rectangle(frame, (x-20,y-20), (x+w+20,y+h+20), (0,0,0), 2)#bounding box
                                # cv2.rectangle(frame, (x-20,y-70), (x+w+22, y-22), (0,0,0), -1) # text background above the bounding box

                                cv2.rectangle(frame, (x-20,y-18), (x+w+20, y+15), (0,0,0), -1) # text background inside the bounding box
                                # cv2.putText(frame, match, (x-18,y-40), font, 0.5, (255,255,255), 1) # text above the bounding box

                                cv2.putText(frame, match, (x-18,y+3), font, 0.5, (0,255,255), 1) # text inside bounding box
                                # cv2.putText(frame, str(dist), (10,15), font, 0.5, (255,0,0), 1) # Display distance threshold

                                font2 = cv2.FONT_HERSHEY_DUPLEX
                                # if z == ord("u"):
                                while True:
                                    # cv2.resize(frame,(480,360))
                                    disp = "Correct recognition Press 'c' "
                                    disp2 = "Incorrect recognition Press 'i' "
                                    disp3="Press 'x' to close this window"
                                    cv2.putText(frame,disp,(70,22), font2, 1, (0,0,0), 1)
                                    cv2.putText(frame,disp2,(70,50), font2, 1, (0,0,0), 1)
                                    cv2.putText(frame,disp3,(60,78), font2, 1, (0,0,0), 1)
                                    cv2.namedWindow('User Confirmation')
                                    cv2.imshow('User Confirmation', frame)
                                    z=cv2.waitKey(1)
                                    if z == ord("c"):
                                        conftxt=[match]+[dist,dt.strftime("%m/%d/%y %H:%M:%S")]+['Yes']
                                        with open('confirmation.csv', 'a') as csvFile:
                                            writer = csv.writer(csvFile)
                                            writer.writerow(conftxt)
                                        csvFile.close()
                                        cv2.destroyWindow('User Confirmation')
                                        break
                                    elif z == ord("i"):
                                        conftxt=[match]+[dist,dt.strftime("%m/%d/%y %H:%M:%S")]+['No']
                                        with open('confirmation.csv', 'a') as csvFile:
                                            writer = csv.writer(csvFile)
                                            writer.writerow(conftxt)
                                        csvFile.close()
                                        cv2.destroyWindow('User Confirmation')
                                        break
                                    elif z == ord("x"):
                                        cv2.destroyWindow('User Confirmation')
                                        break

                            # cv2.rectangle(frame, (x, y), (x+w, y+h), (0,0,0), 1)
                            matched_name=(matching_id.split())
                            csvData = matched_name + [dist,dt.strftime("%d/%m/%y %H:%M:%S")]
                            print(csvData)
                            #open CSV file and load the Name and Time to the 
                            with open('Employee.csv', 'a') as csvFile:
                                writer = csv.writer(csvFile)
                                writer.writerow(csvData)
                            csvFile.close()
                            logging.info('Attendance registered !')
                        except Exception as e:
                            logging.info(e)
                        if matching_id is None:
                            matching_id = 'Unknown'
                            print("Couldn't find match.")
                            # cv2.putText(frame,str('Match not found'), (0, 50), font, 1, (0,0,255), 2, cv2.LINE_AA)
                        else:
                            #if(int(bb[0])<=170 and int(bb[3])<=357 and int(bb[2])<=437 and int(bb[1])<=164):
                            print('Hi %s! Distance: %1.4f' % (matching_id, dist))


                cv2.imshow('Face Recognition',frame)
                k=cv2.waitKey(1)
                if k == ord("q"):
                    break
                print('Rate :>>>',1/(time.time() - timeCheck))
                timeCheck = time.time()

            cv2.destroyAllWindows()

if __name__ == '__main__':
    dt = datetime.now()
    now=dt.strftime("%Y-%m-%d-%H-%M-%S")
    logging.basicConfig( filename=r'Logs/FR_'+str(now)+'.log',level=logging.DEBUG,format = '%(asctime)s  %(levelname)-10s %(processName)s  %(name)s %(message)s %(lineno)d %(funcName)s')
    #logging.disabled = True
    logging.disable(logging.NOTSET) #to enable logging
    #logging.disable(sys.maxsize)   #to disable logging
    logging.info('parsing arguments')
    try:
        parser = argparse.ArgumentParser() #To access the arguments from the command line
        parser.add_argument('model', type=str, help='Path to facenet model', default=r'E:\\Ajinkya\\assets\\Infogen\\FR_Infogen_v3\\server\\Facenetmodel18\\model18.pb')
        # parser.add_argument('-t', '--threshold', type=float,help='Distance threshold defining an id match', default=0.7)# setting up the default threshod value 
    except Exception as e:
        logging.exception(e)

    main(parser.parse_args()) #parse_args() will typically be called with no arguments, and the 'ArgumentParser' will automatically determine the command-line arguments